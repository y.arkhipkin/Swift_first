import Foundation

class Shape {
    func calculateArea() -> Double {
        fatalError("not implemented")
    }
    
    func calculatePerimeter() -> Double {
        fatalError("not implemented")
    }
}

final class Rectangle: Shape {
    private let height: Double
    private let width: Double
    
    init(height: Double, width: Double) {
        self.height = height
        self.width = width
    }
    
    override func calculateArea() -> Double {
        height * width
    }
    
    override func calculatePerimeter() -> Double {
        (height + width) * 2
    }
}

final class Circle: Shape {
    private let radius: Double
    
    init(radius: Double) {
        self.radius = radius
    }
    
    override func calculateArea() -> Double {
        Double.pi * pow(radius, 2)
    }
    
    override func calculatePerimeter() -> Double {
        Double.pi * radius * 2
    }
}

final class Square: Shape {
    private let side: Double
    
    init(side: Double) {
        self.side = side
    }
    
    override func calculateArea() -> Double {
        pow(side, 2)
    }
    
    override func calculatePerimeter() -> Double {
        side * 4
    }
}

let rectangle = Rectangle(height: 3.0, width: 4.0)
let circle = Circle(radius: 2.0)
let square = Square(side: 6.0)

let shapes: [Shape] = [rectangle, circle, square]
var areaCount: Double = 0.0
var perimeterCount: Double = 0.0

for shape in shapes {
    areaCount += shape.calculateArea()
    perimeterCount += shape.calculatePerimeter()

    if shape is Rectangle {
        print("Площадь прямоугольника: \(rectangle.calculateArea())")
        print("Периметр прямоугольника: \(rectangle.calculatePerimeter())")
    } else if shape is Circle {
        print("\nПлощадь круга: \(circle.calculateArea())")
        print("Периметр круга: \(circle.calculatePerimeter())")
    } else if shape is Square {
        print("\nПлощадь квадрата: \(square.calculateArea())")
        print("Периметр квадрата: \(square.calculatePerimeter())")
    }
}

print("\nОбщая площадь всех фигур: \(areaCount)")
print("Общая площадь всех фигур: \(perimeterCount)")