//Задание 1
import Foundation

final class NameList {
    private var namesDictionary: [Character: [String]] = [:]

    func add(name: String) {
        guard let firstLetter = name.first else { return }

        if let names = namesDictionary[firstLetter] {
            var updatedNamesDictionary = names
            updatedNamesDictionary.append(name)
            updatedNamesDictionary.sort()
            namesDictionary[firstLetter] = updatedNamesDictionary
        } else {
            namesDictionary[firstLetter] = [name]
        }
    }

    func printNames() {
        let sortedKeys = namesDictionary.keys.sorted()

        for key in sortedKeys {
            print("\(key)")
            
            if let names = namesDictionary[key] {
                for name in names {
                    print("\(name)")
                }
            }
        }
    }
}

let list = NameList()
list.add(name: "Акулова")
list.add(name: "Валерьянов")
list.add(name: "Бочкин")
list.add(name: "Бабочкина")
list.add(name: "Арбузов")
list.add(name: "Васильева")
list.printNames()
