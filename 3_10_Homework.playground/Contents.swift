//3.10 Домашнее задание №3
//задание 2
func isPrime(_ number: Int) -> Bool {
    if number < 2 {
        return false
    }

func makeBuffer() -> (String) -> Void {
    var storage = ""
    
    return { value in
        if value.isEmpty {
            print(storage)
        } else {
            storage.append(value)
            print(value)
        }
    }
}

isPrime(1) // false, т.к. формально не является простым
isPrime(2) // true
isPrime(3) // true
isPrime(7) // true
isPrime(8) // false
isPrime(99) // false
isPrime(121) // false
isPrime(151) // true
