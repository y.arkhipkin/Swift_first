import UIKit

//4.13 Домашнее задание №4
//Задание 1
struct Device {
    static let iPhone14Pro = Device(
        name: "iPhone 14 Pro",
        screenSize: CGSize(width: 393, height: 852),
        screenDiagonal: 6.1,
        scaleFactor: 3
    )
    
    static let iPhoneXR = Device(
        name: "iPhone XR",
        screenSize: CGSize(width: 414, height: 896),
        screenDiagonal: 6.06,
        scaleFactor: 2
    )
    
    static let iPadPro = Device(
        name: "iPad Pro",
        screenSize: CGSize(width: 834, height: 1194),
        screenDiagonal: 11,
        scaleFactor: 2
    )
    
    let name: String
    let screenSize: CGSize
    let screenDiagonal: Double
    let scaleFactor: Int
    
    func physicalSize() -> CGSize {
        CGSize(
            width: screenSize.width * CGFloat(scaleFactor),
            height: screenSize.height * CGFloat(scaleFactor)
        )
    }
}

let iPhone14ProPhysicalSize = Device.iPhone14Pro.physicalSize()
print("Разрешение iPhone 14 Pro: \(iPhone14ProPhysicalSize)")

let iPhonexrPhysicalSize = Device.iPhoneXR.physicalSize()
print("Разрешение iPhone XR: \(iPhonexrPhysicalSize)")

let iPadProPhysicalSize = Device.iPadPro.physicalSize()
print("Разрешение iPad Pro: \(iPadProPhysicalSize)")
