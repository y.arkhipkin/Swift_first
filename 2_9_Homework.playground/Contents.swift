//Задача 1
var milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

//Задача 2
let modifiedMilkPrice: Int = 3

//Задача 3
var milkPrice = 3.0
milkPrice = 4.2

//Задача 4
var milkBottleCount: Int?
var profit: Double = 0
milkBottleCount = 20
profit = milkPrice * Double(milkBottleCount!)
print(profit)

/*
//Дополнительное задание
if let bottleCount = milkBottleCount {
    profit = milkPrice * Double(bottleCount)
} else {
    print("milkBottleCount == nil")
}
//при работе с опционалами нужно извлекать их безопасно, потому что если значение окажется nil, то программа прекратит свою работу, а так мы просто вывели в консоль, что переменная == nil
*/
 
//Задача 5
var employeesList: [String]
employeesList = ["Иван", "Пётр", "Геннадий", "Марфа", "Андрей"]

//Задача 6
var workingHours = 36
let isEveryoneWorkHard = workingHours >= 40
print(isEveryoneWorkHard)
