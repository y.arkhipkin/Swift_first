func findIndexWithGenerics<T: Equatable>(valueToFind: T, in array: [T]) -> Int? {

    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}
 
let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
let arrayOfDouble = [2.2, 3.4, 5.7, 6.56, 10.03]
let arrayOfInteger = [9, 88, 54, 22, 154]

if let foundIndexString = findIndexWithGenerics(valueToFind: "попугай", in: arrayOfString) {
    print("Индекс попугая: \(foundIndexString)")
}

if let foundIndexDouble = findIndexWithGenerics(valueToFind: 6.56, in: arrayOfDouble) {
    print("Индекс 6.56: \(foundIndexDouble)")
}

if let foundIndexInteger = findIndexWithGenerics(valueToFind: 154, in: arrayOfInteger) {
    print("Индекс 154: \(foundIndexInteger)")
}