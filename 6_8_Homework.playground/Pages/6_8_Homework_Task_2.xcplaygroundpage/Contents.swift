//Домашнее задание №6
//Задание 2
struct Candidate {
    enum Grade {
        case junior
        case middle
        case senior
    }
     
    let grade: Grade
    let requiredSalary: Int
    let fullName: String
}

extension Candidate.Grade {
    var gradeOrder: Int {
        switch self {
        case .junior:
            return 1
        case .middle:
            return 2
        case .senior:
            return 3
        }
    }
}

protocol CandidateFilter {
    func filter(candidates: [Candidate]) -> [Candidate]
}

final class GradeFilter: CandidateFilter {
    private let grade: Candidate.Grade
    
    init(grade: Candidate.Grade) {
        self.grade = grade
    }
    
    func filter(candidates: [Candidate]) -> [Candidate] {
        candidates.filter { candidate in
            candidate.grade == grade
        }
    }
}

final class GradeNumFilter: CandidateFilter {
    private let minGrade: Int
    
    init(minGrade: Int) {
        self.minGrade = minGrade
    }
    
    func filter(candidates: [Candidate]) -> [Candidate] {
        candidates.filter { candidate in
            candidate.grade.gradeOrder >= minGrade
        }
    }
}

final class SalaryFilter: CandidateFilter {
    private let maxSalary: Int
    
    init(maxSalary: Int) {
        self.maxSalary = maxSalary
    }
    
    func filter(candidates: [Candidate]) -> [Candidate] {
        candidates.filter { candidate in
            candidate.requiredSalary <= maxSalary
        }
    }
}

final class NameFilter: CandidateFilter {
    private let nameSubstring: String
    
    init(nameSubstring: String) {
        self.nameSubstring = nameSubstring
    }
    
    func filter(candidates: [Candidate]) -> [Candidate] {
        candidates.filter { candidate in
            candidate.fullName.contains(nameSubstring)
        }
    }
}

let candidates: [Candidate] = [
    Candidate(grade: .junior, requiredSalary: 50000, fullName: "Андрей Сапогов"),
    Candidate(grade: .middle, requiredSalary: 150000, fullName: "Валерий Жижкин"),
    Candidate(grade: .senior, requiredSalary: 300000, fullName: "Иван Петров")
]


let filteredByGrade = GradeFilter(grade: .senior).filter(candidates: candidates)
let filteredByGradeNum = GradeNumFilter(minGrade: 2).filter(candidates: candidates)
let filteredBySalary = SalaryFilter(maxSalary: 200000).filter(candidates: candidates)
let filteredByName = NameFilter(nameSubstring: "ов").filter(candidates: candidates)